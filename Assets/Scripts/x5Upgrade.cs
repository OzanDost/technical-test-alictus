﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class x5Upgrade : MonoBehaviour
{
    public GameManager GM;
    public MakeMoney MM;
    public Text unlocksAtDisplay;
    public Button x5Button;
    public GameObject lockedIcon;
    public int x5Cost = 4000000;
    public int unlocksAtLevel = 8;
    public GameObject durationBarContainer;
    public Image durationBar;
    public bool isBought;
    float duration;
    float cooldown;
    float time;
    public bool onCooldown;
    public GameObject remainingCooldownDisplay;
    public GameObject buttonPanel;
    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        time = 0;
        duration = 30f;
        cooldown = 180;
        onCooldown = false;
        remainingCooldownDisplay.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GM.level < 8)
        {
            x5Button.interactable = false;
            unlocksAtDisplay.text = "Unlocks at level " + unlocksAtLevel;
        }
        else if (onCooldown == false)
        {
            x5Button.interactable = true;
            if (isBought == true)
            {
                buttonPanel.SetActive(false);
            }

        }
        else if (onCooldown == true)
        {
            x5Button.interactable = false;
        }
    }

    public void OpenPanel()
    {
        if (isBought == false)
        {
            buttonPanel.SetActive(true);
        }
    }

    public void OnPurchase()
    {
        if (GM.currentMoney >= x5Cost && isBought == false)
        {
            isBought = true;
            lockedIcon.SetActive(false);
            GM.currentMoney -= x5Cost;
            StartCoroutine(Start5x());
            onCooldown = true;
        }

    }
    public void OnAvtivate()
    {
        if (isBought == true)
        {
            StartCoroutine(Start5x());
            onCooldown = true;
            remainingCooldownDisplay.SetActive(false);
        }
        else
        {
            return;
        }
    }

    public IEnumerator Start5x()
    {
        x5Button.interactable = false;
        durationBarContainer.SetActive(true);
        durationBar.fillAmount = 0;
        MM.SendMessage("X5Tap");
        while (time < duration)
        {
            time += 1;
            durationBar.fillAmount = time / duration;
            yield return new WaitForSeconds(1);
        }

        time = 0;
        StartCoroutine(Cooldown());
        StopCoroutine(Start5x());
        onCooldown = true;
        durationBarContainer.SetActive(false);
        MM.SendMessage("NormalizeTap");


    }
    public IEnumerator Cooldown()
    {
        remainingCooldownDisplay.SetActive(true);
        remainingCooldownDisplay.GetComponent<Text>().text = cooldown.ToString();
        while (cooldown >= 0)
        {
            cooldown -= 1;
            remainingCooldownDisplay.GetComponent<Text>().text = "Cooldown: " + cooldown;
            yield return new WaitForSeconds(1);
        }
        cooldown = 180;
        StopCoroutine(Cooldown());
        x5Button.interactable = true;
        onCooldown = false;
        remainingCooldownDisplay.GetComponent<Text>().text = "Ready!";
    }
}
