﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    public GameManager GM;
    public Image levelBar;
    int nextLevelMoney;
    public TextAsset csv;
    string[,] levelList;
    void Start()
    {
       
        levelList = CSVReader.SplitCsvGrid(csv.text);

        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        nextLevelMoney = System.Int32.Parse(levelList[1,1]);
    }

    void Update()
    {
        incrementLevel();
    }

    public void incrementLevel()
    {
        if(GM.totalMoney >= nextLevelMoney)
        {
            nextLevelMoney = System.Int32.Parse(levelList[1,GM.level +2]);
            GM.level += 1;
           
        } 
    }
}
