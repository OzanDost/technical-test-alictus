﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeMoney : MonoBehaviour
{
    public GameManager GM;
    int outsideModifier;
    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        outsideModifier = 1;
    }
    public void X5Tap()
    {
        outsideModifier = 5;
    }
    public void NormalizeTap()
    {
        outsideModifier = 1;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateMoney()
    {
        GM.totalMoney += GM.moneyPerHit * outsideModifier;
        GM.currentMoney += GM.moneyPerHit * outsideModifier;
    }
}
