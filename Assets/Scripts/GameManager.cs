﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text levelDisplay, moneyPerSecondDisplay, moneyPerHitDisplay, totalMoneyDisplay, currentMoneyDisplay, hoursDisplay, daysDisplay;
    public int totalMoney, currentMoney, moneyPerSecond, moneyPerHit, level;
    public int seconds, hours, days;
    
    // Start is called before the first frame update
    void Start()
    {
        
        moneyPerHit = 1;
        IEnumerator timer = GameTime();
        StartCoroutine(timer);
    }

    // Update is called once per frame
    void Update()
    {
        
        moneyPerHitDisplay.text = "Money Per Hit: " + NumberFormatter(moneyPerHit);
        currentMoneyDisplay.text = "Money: " + NumberFormatter(currentMoney);
        totalMoneyDisplay.text = "Total Money: " + NumberFormatter(totalMoney);
        levelDisplay.text = "Level: " + level;
        hoursDisplay.text = "Hours: " + hours;
        daysDisplay.text = "Days: " + days;

    }

    private void TimeAdd()
    {
        seconds += 1;

        if(seconds >= 15)
        {
            hours += 1;
            seconds = 0;
            if(hours >= 24)
            {
                days += 1;
                hours = 0;
            }
            
        }
    }

    IEnumerator GameTime()
    {
        while (true)
        {
            TimeAdd();
            yield return new WaitForSeconds(1);
        }
    }

    public void PanelToggle(GameObject panel)
    {
        bool isActive = panel.activeSelf;

        panel.SetActive(!isActive);
    }
    public string NumberFormatter(float number)
    {
        if(number >= 1000000000000000)
        {
            return string.Concat("$", (number / 1000000000000000).ToString("0.00"), "Q");
        }
        else if(number >= 1000000000000)
        {
            return string.Concat("$", (number / 1000000000000).ToString("0.00"), "T");
        }
        else if (number >= 1000000000)
        {
            return string.Concat("$", (number / 1000000000).ToString("0.00"), "B");
        }
        else if (number >= 1000000)
        {
            return string.Concat("$", (number / 1000000).ToString("0.00"), "M");
        }
        else if (number >= 1000)
        {
            return string.Concat("$", (number / 1000).ToString("0.00"), "K");
        }
        else
        {
            return string.Concat("$", (int)number);
        }
    }
}
