﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class X2Upgrade : MonoBehaviour
{
    public GameManager GM;
    public GameObject BussinessScripts;
    public Text unlocksAt;
    public Button x2Button;
    public GameObject lockedButton;
    public int x2Cost = 2000000;
    public int unlocksAtLevel = 5;
    public float cooldown;
    public GameObject durationBarContainer;
    public Image durationBar;
    public bool isBought;
    float duration;
    float time;
    public bool onCooldown;
    public GameObject remainingCooldown;
    public GameObject buttonPanel;
    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        time = 0;
        duration = 30f;
        cooldown = 120f;
        onCooldown = false;
        remainingCooldown.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GM.level < 5)
        {
            x2Button.interactable = false;
            unlocksAt.text = "Unlocks at level " + unlocksAtLevel;
        }
        else if(onCooldown==false )
        {
            x2Button.interactable = true;
            if (isBought == true)
            {
                buttonPanel.SetActive(false);
            }

        }
        else if(onCooldown == true)
        {
            x2Button.interactable = false;
        }
    }
    public void OpenPanel()
    {
        if(isBought == false)
        {
            buttonPanel.SetActive(true);
        }
    }

    public void OnPurchase()
    {
        if (GM.currentMoney >= x2Cost && isBought ==false)
        {
            isBought = true;
            lockedButton.SetActive(false);
            GM.currentMoney -= x2Cost;
            StartCoroutine(Start2x());
            onCooldown = true;
        }
        
    }
    public void OnAvtivate()
    {
        if(isBought== true)
        {
            StartCoroutine(Start2x());
            onCooldown = true;
            remainingCooldown.SetActive(false);
        }
        else
        {
            return;
        }
    }
    

    public IEnumerator Start2x()
    {
        x2Button.interactable = false;
        durationBarContainer.SetActive(true);
        durationBar.fillAmount = 0;
        BussinessScripts.BroadcastMessage("SetX2");
        while (time < duration)
        {
            time += 1;
            durationBar.fillAmount = time / duration;
            yield return new WaitForSeconds(1);
        }

        time = 0;
        StartCoroutine(Cooldown());
        StopCoroutine(Start2x());
        onCooldown = true;
        durationBarContainer.SetActive(false);
        BussinessScripts.BroadcastMessage("ProfitNormalize");
        

    }
    public IEnumerator Cooldown()
    {
        remainingCooldown.SetActive(true);
        remainingCooldown.GetComponent<Text>().text = cooldown.ToString();
        while ( cooldown >= 0)
        {
            cooldown -= 1;
            remainingCooldown.GetComponent<Text>().text = "Cooldown: " + cooldown;
            yield return new WaitForSeconds(1);
        }
        cooldown = 120;
        StopCoroutine(Cooldown());
        x2Button.interactable = true;
        onCooldown = false;
        remainingCooldown.GetComponent<Text>().text = "Ready!";
    }
}
