﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoBuy : MonoBehaviour
{
    public GameManager GM;
    public GameObject BussinessScripts;
    public Button autoButton;
    public Text unlocksAt;
    bool isBought;
    public GameObject lockedButton;
    public int autoCost = 500000;
    public int unlocksAtLevel = 3;
    public Sprite tickIcon;

    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GM.level < 3)
        {
            autoButton.interactable = false;
            unlocksAt.text = "Unlocks at level " + unlocksAtLevel;
        }
        else if (isBought == false)
        {
            autoButton.interactable = true;
        }
        else if (isBought == true)
        {
            autoButton.interactable = false;
        }
    }

    public void OnPurchaseAuto()
    {
        
            //autoButton.interactable = true;
        if (GM.currentMoney >= autoCost && isBought == false)
        {
            isBought = true;
            BussinessScripts.BroadcastMessage("SetAuto");
            lockedButton.GetComponent<Image>().sprite = tickIcon;
            GM.currentMoney -= autoCost;
  
        }
        
        
    }
}

