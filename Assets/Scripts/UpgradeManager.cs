﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeManager : MonoBehaviour
{
   
    public GameManager GM;
    public Text tapUpgradeDisplay;
    float currentTapUpgradePrice;
    int outsideModifier;
    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        currentTapUpgradePrice = 20;
        outsideModifier = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   

    public void OnUpgradeTapClick()
    {
        if(GM.currentMoney >= currentTapUpgradePrice)
        {
            GM.currentMoney -= (int)currentTapUpgradePrice;
            GM.moneyPerHit += 1;
            currentTapUpgradePrice = Mathf.Round(currentTapUpgradePrice * 1.07f) * outsideModifier;
            tapUpgradeDisplay.text = "$" + (int)currentTapUpgradePrice;
        }
        
    }
}
