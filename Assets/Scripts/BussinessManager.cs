﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BussinessManager : MonoBehaviour
{
    public GameManager GM;
    public Text levelDisplay;
    public Text costDisplay;
    public Text isBoughtDisplay;
    public Text earnPerCycleDisplay;
    public Text cycleLengthDisplay;
    public Image cycleTimer;
    public GameObject store;

    public bool isAutoActive;
    public int level;
    public int cost;
    public bool isBought;
    public int earnPerCycle;
    public int profitModifier;
    public int costModifier;
    public float maxTime;
    int outsideProfitModifier;
    float time;

    public IEnumerator firstCycle;
    public IEnumerator restCycle;
    
    public Button collectButton;
    public Button bussinessButton;
    // Start is called before the first frame update
    void Start()
    {
        GM = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        outsideProfitModifier = 1;
        firstCycle = CycleTimer();
    }

    // Update is called once per frame
    void Update()
    {
        levelDisplay.text ="lvl: "+ level;
        costDisplay.text = NumberFormatter(cost);
        isBoughtDisplay.text = isBought ? "Upgrade" : "BUY";
        earnPerCycleDisplay.text ="Earns " + NumberFormatter(earnPerCycle) + " per cycle";
    }

    public IEnumerator CycleTimer()
    {
        cycleTimer.fillAmount = 0;
        while (time < maxTime)
        {
            time += .1f;
            cycleTimer.fillAmount = time / maxTime;
            yield return new WaitForSeconds(.1f);
        }
        
        
        //StopCoroutine(firstCycle);
        time = 0;
        StopCoroutine(CycleTimer());
        if (isAutoActive)
        {
            CollectMoney();
        }
        else
        {
            collectButton.gameObject.SetActive(true);
        }
    }

    public void SetAuto()
    {
        isAutoActive = true;
        Debug.Log("I was called");
    }
    public void SetX2()
    {
        outsideProfitModifier = 2;
    }
    public void ProfitNormalize()
    {
        outsideProfitModifier = 1;   
    }
   

    public void CollectMoney()
    {
        
        GM.currentMoney += earnPerCycle * outsideProfitModifier;
        GM.totalMoney += earnPerCycle * outsideProfitModifier;
        StartCoroutine(CycleTimer());
        collectButton.gameObject.SetActive(false);
    }

    public void OnPurchaseOrUpgrade()
    {
        if(GM.currentMoney >= cost && !isBought)
        {
            store.SetActive(true);
            level = 1;           
            isBought = true;
            GM.currentMoney -= cost;
            StartCoroutine(CycleTimer());
            cost += cost / costModifier + Random.Range(-1, 2);
        }
        else if(GM.currentMoney >= cost && isBought && level <= 200)
        {
            level += 1;
            GM.currentMoney -= cost;
            cost += cost / costModifier;

            maxTime = level == 10 ? maxTime / 2 : maxTime;
            maxTime = level == 50 ? maxTime / 2 : maxTime;
            maxTime = level == 100 ? maxTime / 2 : maxTime;
            maxTime = level == 200 ? maxTime / 2 : maxTime;

            if(level == 200)
            {
                isBoughtDisplay.text = "MAX";
                bussinessButton.interactable = false;
            }
        }

        
        earnPerCycle = level * profitModifier;
    }

    public string NumberFormatter(float number)
    {
        if(number >= 1000000000)
        {
            return string.Concat("$", (number / 1000000000).ToString("0.00"), "B");
        }
        else if (number >= 1000000)
        {
            return string.Concat("$", (number / 1000000).ToString("0.00"), "M");
        }
        else if (number >= 1000)
        {
            return string.Concat("$", (number / 1000).ToString("0.00"), "K");
        }
        else
        {
            return string.Concat("$", (int)number);
        }
    }
}
